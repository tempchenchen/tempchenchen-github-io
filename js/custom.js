var start = new Date("2020-01-18 00:00").getTime();

var timer = setInterval(function() {

let now = newDate().getTime();
let t = now - start;

let days = Math.floor(t / (1000 * 60 * 60 * 24));
let hours = Math.floor((t % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
let mins = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
let secs = Math.floor((t % (1000 * 60)) / 1000);

document.getElementById("timer-days").innerHTML = days +
"<span class='label'>DAY(S)</span>";

document.getElementById("timer-hours").innerHTML = ("0"+hours).slice(-2) +
"<span class='label'>HR(S)</span>";

document.getElementById("timer-mins").innerHTML = ("0"+mins).slice(-2) +
"<span class='label'>MIN(S)</span>";

document.getElementById("timer-secs").innerHTML = ("0"+secs).slice(-2) +
"<span class='label'>SEC(S)</span>";


}, 1000);
function diffTime(startDate,endDate) { 
  startDate= new Date(startDate);
  endDate = new Date(endDate);
  var diff=endDate.getTime() - startDate.getTime();//时间差的毫秒数 

  //计算出相差天数 
  var days=Math.floor(diff/(24*3600*1000)); 
      
  //计算出小时数 
  var leave1=diff%(24*3600*1000);    //计算天数后剩余的毫秒数 
  var hours=Math.floor(leave1/(3600*1000)); 
  //计算相差分钟数 
  var leave2=leave1%(3600*1000);        //计算小时数后剩余的毫秒数 
  var minutes=Math.floor(leave2/(60*1000)); 
      
  //计算相差秒数 
  var leave3=leave2%(60*1000);      //计算分钟数后剩余的毫秒数 
  var seconds=Math.round(leave3/1000); 
    
  var returnStr = seconds + " Seconds"; 
  if(minutes>0) { 
      returnStr = minutes + " Minutes " + returnStr; 
  } 
  if(hours>0) { 
      returnStr = hours + " Hours " + returnStr; 
  } 
  if(days>0) { 
      returnStr = days + " Days " + returnStr; 
  } 
  return returnStr; 
  }   
  var currentDate = Date();
  var diffTimeShow = diffTime('2020-01-18 00:00', currentDate);  

